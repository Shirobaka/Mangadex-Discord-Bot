import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.MessageBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.security.auth.login.LoginException;
import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.*;

public class Bot {
    private static String DiscordToken = "";
    private static String Channel = "";

    private static String Username = "";
    private static String Password = "";

    private static final int CheckInterval = 60;

    private static final ArrayList<String> langCodes = new ArrayList<>();

    public static JDA jda = null;
    public static String Token = null;

    public static String databasetype = "";
    public static String sqlitefile = "";

    public static String sql = "";
    public static String user = "";
    public static String pass = "";
    public static Connection sqlCon = null;

    public static void main(String[] args) throws IOException, SQLException {
        Console("Loading config file ...");
        LoadConfig();

        Console("Connecting to discord-server ...");
        try {
            jda = JDABuilder.createDefault(DiscordToken).build();
            jda.awaitReady();
            Console("... discord-server connection has been successfully established.");
        } catch (LoginException | InterruptedException e) {
            e.printStackTrace();
            Console("... the discord-server connection could not be established. Error: " + e.getCause());
            System.exit(-1);
        }

        Console("Connecting to mangadex ...");
        try {
            MDLogin();
            Console("... mangadex connection has been successfully established.");
        } catch (IOException e) {
            e.printStackTrace();
            Console("... the mangadex connection could not be established");
        }

        Console("The bot has been successfully started.");

        Thread MangadexCheck = new Thread(() -> {
            while(true) {
                try {
                    CheckFollows();
                    Thread.sleep(CheckInterval*1000);
                } catch(IOException | InterruptedException | SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        MangadexCheck.start();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String text;
        while ((text = in.readLine()) != null) {
            if (text.equals("bot")) {
                CheckFollows();
            }
        }
    }

    static void LoadConfig()
    {
        Properties prop = new Properties();
        String fileName = "md.config";
        try (FileInputStream fis = new FileInputStream(fileName)) {
            prop.load(fis);
        } catch (FileNotFoundException ex) {
            System.out.println("md.config not found.");
        } catch (IOException ex) {
            ex.getStackTrace();
        }

        Username = prop.getProperty("username");
        Password = prop.getProperty("password");
        String langs = prop.getProperty("languages");
        String[] array = langs.split("\\,", -1);
        for (String s : array) {
            Console(s);
            langCodes.add(s);
        }

        DiscordToken = prop.getProperty("discordtoken");
        Channel = prop.getProperty("discordchannel");

        databasetype = prop.getProperty("database");

        if(databasetype.equals("sqlite"))
        {
            sqlitefile = prop.getProperty("sqlitefile");
        }
        else if(databasetype.equals("mysql"))
        {
            sql = "jdbc:mysql://" + prop.getProperty("host") + ":" + prop.getProperty("port") + "/" + prop.getProperty("db");
            user = prop.getProperty("user");
            pass = prop.getProperty("pass");
        }
        Console("... config file loaded.");
        BuildConnection();
    }

    static void BuildConnection()
    {
        if(databasetype.equals("sqlite"))
        {
            Console("Loading sqlite database file ...");
            try {
                Class.forName("org.sqlite.JDBC");
            } catch (ClassNotFoundException e)
            {
                Console("Class not found");
            }
            try {
                Console("jdbc:sqlite:" + sqlitefile);
                sqlCon = DriverManager.getConnection("jdbc:sqlite:" + sqlitefile);
                Console("... sqlite file has been successfully loaded.");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                Console("... the sqlite file could not be loaded. Error: " + throwables.getMessage());
                System.exit(-1);
            }
        }
        else if(databasetype.equals("mysql"))
        {
            Console("Connecting to database ...");
            try {
                sqlCon = DriverManager.getConnection(sql, user, pass);
                Console("... database connection has been successfully established.");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                Console("... the database connection could not be established. Error: " + throwables.getMessage());
                System.exit(-1);
            }
        }
    }

    static void CheckFollows() throws IOException, SQLException {
        if(CheckToken()) {
            StringBuilder langString = new StringBuilder("translatedLanguage[]=");
            for (String langCode : langCodes) {
                if (langString.toString().equals("translatedLanguage[]=")) {
                    langString.append(langCode);
                } else {
                    langString.append("&translatedLanguage[]=").append(langCode);
                }
            }
            URL url = new URL("https://api.mangadex.org/user/follows/manga/feed?limit=30&" + langString + "&order[publishAt]=desc");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Authorization", "Bearer " + Token);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output;
            while ((output = in.readLine()) != null) {
                JSONObject json = new JSONObject(output);
                JSONArray data = json.getJSONArray("data");
                for (int r = 0; r < data.length(); r++) {
                    JSONObject object0 = data.getJSONObject(r);
                    JSONArray relationships = object0.getJSONArray("relationships");
                    Object mangaid = null;
                    for (int i = 0; i < relationships.length(); i++) {
                        JSONObject wert = relationships.getJSONObject(i);
                        if (wert.get("type").equals("manga")) {
                            mangaid = wert.get("id");
                        }
                    }

                    Object chapter = object0.getJSONObject("attributes").get("chapter");
                    Object volume = null;
                    if (object0.getJSONObject("attributes").has("volume")) {
                        volume = object0.getJSONObject("attributes").get("volume");
                    }
                    Object cid = object0.get("id");
                    Statement sstmt = sqlCon.createStatement();
                    String squery = "SELECT count(*) FROM bot WHERE chapterid LIKE '" + cid + "'";
                    ResultSet rs = sstmt.executeQuery(squery);
                    int crs = 0;
                    while (rs.next()) {
                        crs = rs.getInt(1);
                    }
                    if (crs == 0) {
                        assert mangaid != null;
                        Object Name = GetManga(mangaid.toString(), "Name");
                        Object Thumbnail = GetManga(mangaid.toString(), "Thumbnail");
                        Object Description = GetManga(mangaid.toString(), "Description");
                        EmbedBuilder eb = new EmbedBuilder();
                        assert Name != null;
                        StringBuilder volcap = new StringBuilder();
                        assert volume != null;
                        if (volume.equals("null")) volcap.append("Vol. ").append(volume).append(" ");
                        volcap.append("Ch. ").append(chapter);
                        eb.setTitle(volcap + " (" + Name + ") - MangaDex", "https://mangadex.org/chapter/" + cid + "/1");
                        eb.setColor(Color.WHITE);
                        assert Description != null;
                        String shortDesc = Description.toString();
                        if (shortDesc.length() > 345) shortDesc = shortDesc.substring(0, 345);
                        eb.setDescription(shortDesc);
                        assert Thumbnail != null;
                        eb.setThumbnail(String.valueOf(new URL("https://uploads.mangadex.org/covers/" + mangaid + "/" + Thumbnail + ".256.jpg")));
                        Objects.requireNonNull(jda.getTextChannelById(Channel)).sendMessage(new MessageBuilder().append("**:newspaper: | ").append(Name).append(" - Chapter ").append(chapter).append("**\n").setEmbed(eb.build()).build()).queue();
                        Statement stmt = sqlCon.createStatement();
                        String query = "INSERT INTO bot (chapterid) VALUES ('" + cid + "')";
                        stmt.executeUpdate(query);
                    }
                }
            }
            in.close();
        }
    }

    static Object GetManga(String mangaid, String type) throws IOException {
        URL url = new URL("https://api.mangadex.org/manga/" + mangaid);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String output = in.readLine();

        if (type.equals("Name")) {
            JSONObject json = new JSONObject(output);
            JSONObject data = json.getJSONObject("data");
            JSONObject attributes = data.getJSONObject("attributes");
            Object title;
            if(attributes.getJSONObject("title").has("en"))
                title = attributes.getJSONObject("title").get("en");
            else
                title = attributes.getJSONObject("title").get("jp");
            return title;
        }

        if (type.equals("Thumbnail")) {
            JSONObject json = new JSONObject(output);
            JSONObject data = json.getJSONObject("data");
            JSONArray relationships = data.getJSONArray("relationships");

            Object coverid = null;
            for (int i = 0; i < relationships.length(); i++)
            {
                JSONObject wert = relationships.getJSONObject(i);
                if(wert.get("type").equals("cover_art"))
                {
                    coverid = wert.get("id");
                }
            }

            URL urlC = new URL("https://api.mangadex.org/cover/"+coverid);

            HttpURLConnection connc = (HttpURLConnection) urlC.openConnection();
            connc.setRequestProperty("Content-Type", "application/json");
            connc.setRequestMethod("GET");

            BufferedReader cin = new BufferedReader(new InputStreamReader(connc.getInputStream()));
            String coutput = cin.readLine();

            JSONObject coutputob = new JSONObject(coutput);
            JSONObject cdata = coutputob.getJSONObject("data");
            return cdata.getJSONObject("attributes").get("fileName");
        }
        if(type.equals("Description"))
        {
            JSONObject json = new JSONObject(output);
            JSONObject data = json.getJSONObject("data");
            JSONObject attributes = data.getJSONObject("attributes");
            return attributes.getJSONObject("description").get("en");
        }
        return null;
    }

    static boolean CheckToken() throws IOException {
        URL url = new URL("https://api.mangadex.org/auth/check");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Authorization", "Bearer " + Token);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestMethod("GET");
        int code = conn.getResponseCode();
        if(code == 200) {

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output = in.readLine();

            JSONObject json = new JSONObject(output);
            Boolean auth = (Boolean) json.get("isAuthenticated");
            if (!auth) MDLogin();
            return true;
        }
        return false;
    }

    static void MDLogin() throws IOException {
        URL MDLogin = new URL("https://api.mangadex.org/auth/login");

        HttpURLConnection con = (HttpURLConnection) MDLogin.openConnection();
        con.setRequestMethod("POST");

        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");

        con.setDoOutput(true);

        String jsonInputString = new JSONObject()
                .put("password", Password)
                .put("username", Username)
                .toString();
        try (OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            JSONObject rstring = new JSONObject(response.toString());
            Object ttstring = rstring.get("token");
            JSONObject tstring = new JSONObject(ttstring.toString());
            Token = tstring.get("session").toString();
        }
    }

    static void Console(Object text) {
        System.out.println(text);
    }
}